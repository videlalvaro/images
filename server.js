var express = require('express');
var amqp    = require('amqplib');
var sockjs  = require('sockjs');
var http    = require('http');
var path    = require('path');
var fs      = require('fs');

//rabbitmq connection
var m_conn  = null;

function serve_image(req, res, type) {
    file = req.params.file;
    var img = fs.readFileSync(__dirname + "/uploads/" + type + "/" + file);
    res.writeHead(200, {'Content-Type': 'image/jpg' });
    res.end(img, 'binary');
}

function display_image(msg) {
    var body = msg.content.toString();
    console.log(" [x] Received '%s'", body);
    broadcast(body);
}

//sockjs clients
var clients = {};

function broadcast(message) {
    for (var i in clients) {
        clients[i].write(message);
    }
}

var sockjs_opts = {sockjs_url: "http://cdn.sockjs.org/sockjs-0.3.min.js"};
var sockjs_srv = sockjs.createServer(sockjs_opts);

sockjs_srv.on('connection', function(conn) {
    clients[conn.id] = conn;
    
    conn.on('close', function() {
        delete clients[conn.id];
    });
});

// 2. Express server
var app = express(); /* express.createServer will not work here */
var server = http.createServer(app);

sockjs_srv.installHandlers(server, {prefix:'/images'});

app.configure(function(){
    app.use(express.bodyParser( { keepExtensions: true, uploadDir: __dirname + '/uploads' } ));
});

// server sends index.html for the homepage
app.get('/', function (req, res) {
    res.sendfile(__dirname + '/index.html');
});

app.post('/upload', function(req, res) {
    fs.readFile(req.files.image.path, function (err, data) {
        var imageName = path.basename(req.files.image.path);

        /// If there's an error
        if(!imageName){
            console.log("There was an error")
            res.redirect("/");
            res.end();
        } else {
            var newPath = __dirname + "/uploads/fullsize/" + imageName;
            var thumbPath = __dirname + "/uploads/thumbs/" + imageName;

            /// write file to uploads/fullsize folder
            fs.writeFile(newPath, data, function (err) {
                m_conn.createChannel().then(function(ch) {
                    var msg = {'img_path': newPath, 'img_name': imageName};
                    // resize exchange
                    // empty routing key
                    // JSON message
                    ch.publish('resize', '', new Buffer(JSON.stringify(msg)));
                    ch.close();
                });
                res.end();
            });
        }
    });
});

// Show files
app.get('/uploads/fullsize/:file', function (req, res){
    serve_image(req, res, "fullsize");
});

app.get('/uploads/thumbs/:file', function (req, res){
    serve_image(req, res, "thumbs");
});


amqp.connect('amqp://localhost').then(function(conn) {
    m_conn = conn;
    return conn.createChannel().then(function(ch) {
        var ex = 'image_ready';
    
        var ok = ch.assertExchange(ex, 'direct', {durable: true});
        ok = ok.then(function() {
            return ch.assertQueue('', {exclusive: true});
        });
    
        ok = ok.then(function(qok) {
            var queue = qok.queue;
            ch.bindQueue(queue, ex, '');
            return queue;
        });

        ok = ok.then(function(queue) {
            return ch.consume(queue, display_image, {noAck: true});
        });
    
        return ok.then(function() {
            // Web Server App
            console.log(' [*] Listening on 0.0.0.0:9999' );
            server.listen(9999, '0.0.0.0');
            return 'started';
        });
    });
}).then(null, console.warn);












