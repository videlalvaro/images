var amqp = require('amqplib');
var im   = require('imagemagick');

amqp.connect('amqp://localhost').then(function(conn) {
  process.once('SIGINT', function() { conn.close(); });
  
  var queue = 'resize_q';
  var imgs_ex = 'resize';
  
  return conn.createChannel().then(function(ch) {

    var ok = ch.assertQueue(queue, {durable: true});
    var ok = ch.assertExchange(imgs_ex, 'direct', {durable: true});
    var ok = ch.bindQueue(queue, imgs_ex, '');

    ok = ok.then(function() { ch.prefetch(1); });

    ok = ok.then(function() {
      ch.consume(queue, resize_image, {noAck: false});
      console.log(" [*] Waiting for messages. To exit press CTRL+C");
    });

    return ok;

    function resize_image(msg) {
      var body = msg.content.toString();
      console.log(" [x] Received '%s'", body);
      var img_data = JSON.parse(body);
      
      // resize image here
      var imgPath = img_data['img_path'];
      var thumbPath = __dirname + "/uploads/thumbs/" + img_data['img_name'];
      
      /// write file to uploads/thumbs folder
      im.resize({
          srcPath: imgPath,
          dstPath: thumbPath,
          width:   200
      }, function(err, stdout, stderr){
          if (err) {
              console.log('image resize failed!');
          } else {
              console.log('resized image to fit within 200x200px');
              // publish image to next filter in the chain 'image_ready'
              var result = JSON.stringify({'image': imgPath, 'thumb': thumbPath});
              ch.publish('image_ready', '', new Buffer(result));
              ch.ack(msg);
          }
      });
    }
  });
}).then(null, console.warn);